process.on('unhandledRejection', (reason, p) =>
{
	console.log('Unhandled Rejection at: Promise', p, 'reason:', reason); // for debugging purposes
});

const request = require('request-promise-native');
const fs = require('fs');
const readline = require('readline');

async function main()
{
	if(!fs.existsSync('files.json'))
	{
		console.log('files.json을 찾을 수 없습니다!');
		console.log('토스캠프에서 프로그램을 다시 받아 주세요!');
		process.exit();
	}
	
	const files = JSON.parse(fs.readFileSync('files.json'));
	
	await displayDescription();
	
	let location;
	if(fs.existsSync('C:\\Program Files (x86)\\Steam\\steamapps\\common\\TreeOfSavior'))
	{
		console.log('기본 설치 경로에 게임 클라이언트가 설치되어있는 것을 발견했습니다!');
		location = 'C:\\Program Files (x86)\\Steam\\steamapps\\common\\TreeOfSavior';
	}
	else
	{
		console.log('트리 오브 세이비어 클라이언트 자동 감지에 실패하였습니다!');
		console.log('클라이언트 설치 경로를 입력하고 엔터를 눌러주세요!');
		console.log('(폴더명 TreeOfSavior, 내부에 release 폴더가 있어야 합니다.)');
		while(true)
		{
			location = await question('설치 경로 입력: ');
			if(fs.existsSync(location)) break;
			console.log('경로가 올바르지 않습니다! 다시 시도해 주세요.');
		}
	}
	
	process.stdout.write('패치 진행 중... ');
	
	if(fs.existsSync(`${location}\\release\\languageData\\한국어`))
	{
		await deleteFolderRecursive(`${location}\\release\\languageData\\한국어`);
	}
	fs.mkdirSync(`${location}\\release\\languageData\\한국어`);
	const KR = location + '\\release\\languageData\\한국어';
	const EN = location + '\\release\\languageData\\English';
	
	await (async() =>
	{
		for(let index = 0; index < files.length; index++)
		{
			const fileName = files[index][0];
			const url = files[index][1];
			{
				const KRData = await downloadFile(url);
				let ENData = await parseFile(fileName, EN);
				await Object.assign(ENData, KRData);
				const data = await serializeData(ENData);
				saveFile(fileName, KR, data);
			}
		}
	})();
	
	console.log('완료!');
	console.log('10초 후 자동 종료됩니다...');
	setTimeout(()=>{process.exit();}, 10000);
}

const displayDescription = async () =>
{
	console.log('스팀 트오세 한글패쳐 v1.0');
	console.log('사용 전, 토스캠프에서 새로운 버전의 패쳐가 있는지 꼭 확인해주세요!');
	console.log('');
	console.log('경고! VAC밴의 위험이 있습니다!');
	console.log('스팀과 트오세 클라이언트를 반드시 종료시키고 진행해주세요!');
	console.log('이 프로그램의 사용으로 발생할 수 있는 어떤 문제도 개발자는 책임지지 않습니다!');
	console.log();
	const answer = await question('계속하시겠습니까? (Y/N): ');
	if(answer.toUpperCase() !== 'Y')
	{
		console.log('취소합니다...');
		process.exit();
	}
	console.log();
};

const question = async (question) =>
{
	const rl = readline.createInterface(
		{
			input: process.stdin,
			output: process.stdout
		}
	);
	return new Promise((resolve, reject) =>
	{
		rl.question(question, (answer) =>
		{
			resolve(answer);
		});
	});
};

const downloadFile = async (url) =>
{
	const original = (await request(url)).split('\n');
	
	let parsed = {};
	
	for(let index = 0; index < original.length; index++)
	{
		const line = (original[index]).split('\t');
		parsed[line[0]] = line[2];
	}
	
	return parsed;
};

const saveFile = async (fileName, location, data) =>
{
	const saveLocation = location + '\\' + fileName;
	fs.writeFile(saveLocation, data, () => {});
};

const parseFile = async (fileName, location) =>
{
	let original = (fs.readFileSync(`${location}\\${fileName}`, {encoding: 'utf8'})).split('\n');
	
	let parsed = {};
	
	for(let index = 0; index < original.length; index++)
	{
		const line = (original[index]).split('\t');
		parsed[line[0]] = line[1];
	}
	
	return parsed;
};

const serializeData = async (object) =>
{
	let string = '\n';
	
	for(let key in object)
	{
		string += `${key}\t${object[key]}\n`;
	}
	
	return string;
};

const deleteFolderRecursive = async (path) =>
{
	if (fs.existsSync(path))
	{
		fs.readdirSync(path).forEach(function(file, index)
		{
			var curPath = path + "/" + file;
			if (fs.lstatSync(curPath).isDirectory())
			{
				deleteFolderRecursive(curPath);
			}
			else
			{
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(path);
	}
};

main();